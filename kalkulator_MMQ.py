''' Program kalkulator sederhana untuk menjumlah,
mengurang, mengali, dan memabagi bilangan dengan menggunakan fungsi '''
# fungsi penjumlahan
def add(x, y):
  return x + y
# fungsi pengurangan
def subtract(x, y):
  return x - y
# fungsi perkalian
def multiply (x, y):
  return x * y
# fungsi pembagian
def divide (x , y):
  return x / y
#menu operasi
print("pilihan operasi.")
print("1.jumlah")
print("2.kurang")
print("3.kali")
print("4.bagi")
# Meminta input dari user
choice = str(input("Masukkan pilihan (1/2/3/4):"))
num1 = int(input("Masukkan bilangan pertama: "))
num2 = int(input("Masikan bilangan kedua: "))
if choice == '1':
	print(str(num1)+"+"+str(num2)+"="+str(add(num1,num2)))
elif choice == '2':
	print(str(num1)+"-"+str(num2)+"="+str(subtract(num1,num2)))
elif choice == '3':
	print(str(num1)+"*"+str(num2)+"="+str(multiply(num1,num2)))
elif choice == '4':
	print(str(num1)+"/"+str(num2)+"="+str(divide(num1,num2)))
else:
	print("Input salah")
